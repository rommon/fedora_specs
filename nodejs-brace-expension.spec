%{?nodejs_find_provides_and_requires}

Name:       nodejs-brace-expansion
Version:    1.1.0
Release:    1%{?dist}
Summary:    Brace expansion, as known from sh/bash, in JavaScript
License:    MIT
Group:      System Environment/Libraries
URL:        https://github.com/juliangruber/brace-expansion
Source0:    http://registry.npmjs.org/brace-expansion/-/brace-expansion-%{version}.tgz
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:  noarch
ExclusiveArch: %{nodejs_arches} noarch

BuildRequires:  nodejs-devel

%description
Brace expansion, as known from sh/bash, in JavaScript.

%prep
%setup -q -n package

%build
#nothing to do

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{nodejs_sitelib}/brace-expansion
cp -pr index.js example.js package.json %{buildroot}%{nodejs_sitelib}/brace-expansion

%nodejs_symlink_deps

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{nodejs_sitelib}/brace-expansion
%doc README.md

%changelog
* Sun Apr 12 2015 rommon <rommon@t-online.de> - 1.1.0-1
- initial package
