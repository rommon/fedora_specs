%{?nodejs_find_provides_and_requires}

Name:           nodejs-editor
Version:        0.1.0
Release:        1%{?dist}
Summary:        Launch the default text editor from Node.js programs
BuildArch:      noarch
ExclusiveArch: %{nodejs_arches} noarch

Group:          Development/Libraries
#package.json indicates MIT, but no license file included
#upstream notified in https://github.com/substack/node-editor/pull/5
#we're including a copy of the MIT license based off a copy from another
#project by the same author indicating the same license in order to comply
#with the terms of the MIT license
License:        MIT
URL:            https://github.com/substack/node-editor
Source0:        http://registry.npmjs.org/editor/-/editor-%{version}.tgz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  nodejs-devel

%description
%{summary}.

%prep
%setup -q -n package

%build
#nothing to do

%install
rm -rf %buildroot

mkdir -p %{buildroot}%{nodejs_sitelib}/editor
cp -pr index.js package.json %{buildroot}%{nodejs_sitelib}/editor

%nodejs_symlink_deps

%clean
rm -rf %buildroot

%files
%defattr(-,root,root,-)
%{nodejs_sitelib}/editor
%doc README.markdown LICENSE example

%changelog
* Mon Apr 13 2015 rommon <rommon@t-online.de> - 0.1.0-1
- update to 0.1.0

* Sun Apr 12 2015 rommon <rommon@t-online.de> - 0.0.5-1
- update to 0.0.5

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.0.4-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.0.4-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Sat Jun 22 2013 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.0.4-2
- restrict to compatible arches

* Thu May 30 2013 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.0.4-1
- initial package
