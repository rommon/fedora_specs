%{?nodejs_find_provides_and_requires}

Name:       nodejs-init-package-json
Version:    1.4.0
Release:    1%{?dist}
Summary:    A node module to get your node module started
License:    MIT
Group:      System Environment/Libraries
URL:        https://github.com/npm/init-package-json
Source0:    http://registry.npmjs.org/init-package-json/-/init-package-json-%{version}.tgz
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:  noarch
ExclusiveArch: %{nodejs_arches} noarch

BuildRequires:  nodejs-devel

%description
A node module to get your node module started.

%prep
%setup -q -n package

%build
#nothing to do

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{nodejs_sitelib}/init-package-json
cp -pr *.js package.json %{buildroot}%{nodejs_sitelib}/init-package-json

%nodejs_symlink_deps

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{nodejs_sitelib}/init-package-json
%doc README.md LICENSE example

%changelog
* Mon Apr 13 2015 rommon <rommon@t-online.de> - 1.4.0-1
- update to 1.4.0

* Sun Apr 12 2015 rommon <rommon@t-online.de> - 0.0.14-1
- initial package
