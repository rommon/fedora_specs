# This macro is needed at the start for building on EL6
%{?nodejs_find_provides_and_requires}

%global enable_tests 0

%global barename readable-stream

Name:               nodejs-readable-stream
Version:            1.1.13
Release:            1%{?dist}
Summary:            Streams2, a user-land copy of the stream library from Node.js v0.10.x

Group:              Development/Libraries
License:            MIT
URL:                https://www.npmjs.org/package/readable-stream
Source0:            http://registry.npmjs.org/%{barename}/-/%{barename}-%{version}.tgz
BuildArch:          noarch
%if 0%{?fedora} >= 19
ExclusiveArch:      %{nodejs_arches} noarch
%else
ExclusiveArch:      %{ix86} x86_64 %{arm} noarch
%endif

BuildRequires:      nodejs-packaging >= 6

BuildRequires:      npm(core-util-is)
BuildRequires:      npm(debuglog)

Requires:           npm(core-util-is)
Requires:           npm(debuglog)

%if 0%{?enable_tests}
BuildRequires:      npm(tap)
%endif


%description
This package is a mirror of the Streams2 and Streams3 implementations in
Node-core.

%prep
%setup -q -n package

# Remove bundled node_modules if there are any..
rm -rf node_modules/

%nodejs_fixdep --caret

%build
%nodejs_symlink_deps --build

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/readable-stream
cp -pr package.json lib *.js %{buildroot}%{nodejs_sitelib}/readable-stream

%nodejs_symlink_deps

%check
%if 0%{?enable_tests}
%nodejs_symlink_deps --check
tap test/simple/*.js
%endif

%files
%doc README.md LICENSE
%{nodejs_sitelib}/readable-stream/

%changelog
* Mon Apr 13 2015 rommon <rommon@t-online.de> - 1.1.13-1
- update to 1.1.13

* Tue Jul 22 2014 Ralph Bean <rbean@redhat.com> - 1.1.9-1
- Initial packaging for Fedora.
