%{?nodejs_find_provides_and_requires}

Name:       node-gyp
Version:    1.0.3
Release:    1%{?dist}
Summary:    Node.js native addon build tool
License:    MIT
Group:      System Environment/Libraries
URL:        https://github.com/TooTallNate/node-gyp
Source0:    http://registry.npmjs.org/node-gyp/-/node-gyp-%{version}.tgz
Source1:    addon-rpm.gypi
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:  noarch
ExclusiveArch: %{nodejs_arches} noarch

# These patches are Fedora-specific for the moment, although I'd like to find
# a way to support this kind of stuff upstream.

# use RPM installed headers by default instead of downloading a source tree
# for the currently running node version
Patch1:     node-gyp-addon-gypi.patch

# use the system gyp
Patch2:     node-gyp-system-gyp.patch

BuildRequires:  nodejs-devel

#gyp is the actual build framework node-gyp uses
Requires: gyp
#this is the standard set of headers expected to build any node native module
Requires: nodejs-devel v8-devel libuv-devel http-parser-devel
#we also need a C++ compiler to actually build stuff ;-)
Requires: gcc-c++

%description
node-gyp is a cross-platform command-line tool written in Node.js for compiling
native addon modules for Node.js, which takes away the pain of dealing with the
various differences in build platforms. It is the replacement to the node-waf
program which is removed for node v0.8.

%prep
%setup -q -n package

%nodejs_fixdep request 2.x
%nodejs_fixdep semver 2.1

%build
#nothing to do

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{nodejs_sitelib}/node-gyp
cp -pr addon*.gypi bin lib package.json %{buildroot}%{nodejs_sitelib}/node-gyp
cp -p %{SOURCE1} %{buildroot}%{nodejs_sitelib}/node-gyp/addon-rpm.gypi

mkdir -p %{buildroot}%{_bindir}
ln -sf ../lib/node_modules/node-gyp/bin/node-gyp.js %{buildroot}%{_bindir}/node-gyp

%nodejs_symlink_deps

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{nodejs_sitelib}/node-gyp
%{_bindir}/node-gyp
%doc README.md LICENSE

%changelog
* Sun Apr 12 2015 rommon <rommon@t-online.de> - 1.0.3-1
- updated to 1.0.3

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.10.6-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Tue Jul 30 2013 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.10.6-2
- fix semver dep

* Fri Jul 12 2013 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.10.6-1
- new upstream release 0.10.6

* Sat Jun 22 2013 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.10.1-1
- new upstream release 0.10.1

* Sat Jun 22 2013 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.9.5-3
- restrict to compatible arches

* Mon Apr 15 2013 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.9.5-2
- add macro for EPEL6 dependency generation

* Wed Apr 03 2013 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.9.5-1
- new upstream release 0.9.5

* Wed Mar 13 2013 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.9.1-2
- update addon-rpm.gypi
- split out addon-rpm.gypi so it's easier to maintain

* Wed Mar 13 2013 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.9.1-1
- new upstream release 0.9.1

* Sat Feb 09 2013 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.8.4-1
- new upstream release 0.8.4

* Mon Jan 21 2013 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.8.3-1
- new upstream release 0.8.3
- add missing Requires on http-parser-devel

* Tue Jan 08 2013 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.8.2-3
- add missing build section

* Sat Jan 05 2013 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.8.2-2
- use RPM-installed headers by default
- now patched to use the system gyp instead of relying on a symlink

* Mon Dec 31 2012 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.8.2-1
- new upstream release 0.8.2
- clean up for submission

* Thu Apr 26 2012 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.4.1-2
- fix dependencies

* Wed Apr 18 2012 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.4.1-1
- New upstream release 0.4.1

* Fri Apr 06 2012 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.3.11-1
- New upstream release 0.3.11

* Mon Apr 02 2012 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.3.10-1
- New upstream release 0.3.10

* Thu Mar 29 2012 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.3.9-1
- New upstream release 0.3.9

* Wed Mar 28 2012 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.3.8-1
- new upstream release 0.3.8

* Thu Mar 22 2012 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.3.7-1
- new upstream release 0.3.7

* Thu Mar 15 2012 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.3.5-1
- initial package
