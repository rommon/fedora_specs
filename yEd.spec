Name:           yed
Version:        3.14.1
Release:        1%{?dist}
Summary:        yEd is a powerful desktop application that can be used to quickly and effectively generate high-quality diagrams.
Group:          Office/Tools
License:        yEd Software License Agreement
URL:            http://www.yworks.com/en/products/yfiles/yed/
Source0:        http://www.yworks.com/en/products_download.php?file=yEd-3.14.1.zip
BuildArch:      noarch
Requires:       java

%description
yEd is a powerful desktop application that can be used to quickly and effectively generate high-quality diagrams.
Create diagrams manually, or import your external data for analysis. Our automatic layout algorithms arrange even large data sets with just the press of a button.

%prep
%setup -q -n %{name}-%{version}

%install
mkdir -p %{buildroot}%{_javadir}/%{name}
mkdir -p %{buildroot}%{_datadir}/applications
mkdir -p %{buildroot}%{_datadir}/pixmaps
mkdir -p %{buildroot}%{_bindir}

cp -arf ./lib %{buildroot}%{_javadir}/%{name}/
cp -arf ./*.html %{buildroot}%{_javadir}/%{name}/
cp -arf ./*.jar %{buildroot}%{_javadir}/%{name}/
cp -arf ./icons/{*.ico,*.png} %{buildroot}%{_datadir}/pixmaps/

cat <<EOT >> %{buildroot}%{_javadir}/%{name}/yed.sh
#!/bin/bash
java -jar %{_javadir}/%{name}/yed.jar
EOT

cat <<EOT >> %{buildroot}%{_datadir}/applications/%{name}.desktop
[Desktop Entry]
Name=yEd
Type=Application
Categories=Office
Exec=yed
Icon=yicon32.png
Keywords=Graphs;
EOT

ln -s %{_javadir}/%{name}/%{name}.sh %{buildroot}%{_bindir}/%{name}



%files
%defattr(-,root,root)
%dir %{_javadir}/%{name}
%attr(644,root,root)%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/yicon.ico
%{_datadir}/pixmaps/yicon16.png
%{_datadir}/pixmaps/yicon32.png
%attr(755,root,root) %{_javadir}/%{name}/%{name}.sh
%{_javadir}/%{name}/*
%{_bindir}/%{name}
