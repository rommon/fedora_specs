Name:           LanguageTool
Version:        2.9
Release:        2%{?dist}
Summary:        LanguageTool is an Open Source proof­reading soft­ware for English, French, German, Polish, and more than 20 other languages.
Group:          Office/Tools
License:        LGPL 2.1
URL:            https://www.languagetool.org/
Source0:        https://www.languagetool.org/download/LanguageTool-2.9.zip
Source1:        LanguageTool.png
Source2:        LanguageTool.sh
Source3:        LanguageTool.desktop
Source4:        LanguageTool.appdata.xml
BuildArch:      noarch
BuildRequires:  desktop-file-utils
Requires:       java

%description
LanguageTool is an Open Source proof­reading soft­ware for English, French, German, Polish, and more than 20 other languages.
It finds many errors that a simple spell checker cannot detect and several grammar problems.

%prep
%setup -q -n %{name}-%{version}

%install
mkdir -p %{buildroot}%{_javadir}/%{name}
mkdir -p %{buildroot}%{_datadir}/applications
mkdir -p %{buildroot}%{_datadir}/pixmaps
mkdir -p %{buildroot}%{_datadir}/appdata
mkdir -p %{buildroot}%{_bindir}

cp -arf ./{META-INF,libs,org,third-party-licenses} %{buildroot}%{_javadir}/%{name}/
cp -arf ./*.txt %{buildroot}%{_javadir}/%{name}/
cp -arf ./*.jar %{buildroot}%{_javadir}/%{name}/
cp -arf ./*.sh %{buildroot}%{_javadir}/%{name}/
cp -af %{SOURCE1} %{buildroot}%{_datadir}/pixmaps/%{name}.png
cp -af %{SOURCE2} %{buildroot}%{_javadir}/%{name}/
cp -af %{SOURCE3} %{buildroot}%{_datadir}/languagetool.desktop
cp -a %{SOURCE4} %{buildroot}%{_datadir}/appdata
ln -s %{_javadir}/%{name}/%{name}.sh %{buildroot}%{_bindir}/%{name}

desktop-file-install                          \
--add-category="Office"                  \
--delete-original                             \
--dir=%{buildroot}%{_datadir}/applications    \
%{buildroot}%{_datadir}/languagetool.desktop

%files
%defattr(-,root,root)
%dir %{_javadir}/%{name}
%attr(644,root,root)%{_datadir}/applications/languagetool.desktop
%{_datadir}/appdata/%{name}.appdata.xml
%{_datadir}/pixmaps/%{name}.png
%attr(755,root,root) %{_javadir}/%{name}/%{name}.sh
%{_javadir}/%{name}/*
%{_bindir}/%{name}
