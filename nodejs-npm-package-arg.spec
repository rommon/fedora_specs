%{?nodejs_find_provides_and_requires}

Name:       nodejs-npm-package-arg
Version:    4.0.0
Release:    1%{?dist}
Summary:    Tests whether one path is inside another path
License:    MIT
Group:      System Environment/Libraries
URL:        https://github.com/npm/npm-package-arg
Source0:    http://registry.npmjs.org/npm-package-arg/-/npm-package-arg-%{version}.tgz
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:  noarch
ExclusiveArch: %{nodejs_arches} noarch

BuildRequires:  nodejs-devel

%description
Tests whether one path is inside another path.

%prep
%setup -q -n package

%build
#nothing to do

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{nodejs_sitelib}/npm-package-arg
cp -pr npa.js package.json %{buildroot}%{nodejs_sitelib}/npm-package-arg

%nodejs_symlink_deps

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{nodejs_sitelib}/npm-package-arg
%doc README.md LICENSE

%changelog
* Sun Apr 12 2015 rommon <rommon@t-online.de> - 4.0.0-1
- initial package
