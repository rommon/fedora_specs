%{?nodejs_find_provides_and_requires}

Name:       nodejs-github-url-from-username-repo
Version:    1.0.2
Release:    1%{?dist}
Summary:    Create urls from username/repo
License:    MIT
Group:      System Environment/Libraries
URL:        https://github.com/robertkowalski/github-url-from-username-repo
Source0:    http://registry.npmjs.org/github-url-from-username-repo/-/github-url-from-username-repo-%{version}.tgz
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:  noarch
ExclusiveArch: %{nodejs_arches} noarch

BuildRequires:  nodejs-devel

%description
Create urls from username/repo.

%prep
%setup -q -n package

%build
#nothing to do

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{nodejs_sitelib}/github-url-from-username-repo
cp -pr index.js package.json %{buildroot}%{nodejs_sitelib}/github-url-from-username-repo

%nodejs_symlink_deps

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{nodejs_sitelib}/github-url-from-username-repo
%doc README.md LICENSE

%changelog
* Mon Apr 13 2015 rommon <rommon@t-online.de> - 1.0.2-1
- update to 1.0.2

* Sun Apr 12 2015 rommon <rommon@t-online.de> - 0.0.2-1
- initial package
