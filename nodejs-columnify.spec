%{?nodejs_find_provides_and_requires}

Name:       nodejs-columnify
Version:    1.5.1
Release:    1%{?dist}
Summary:    Render data in text columns. supports in-column text-wrap
License:    BSD
Group:      System Environment/Libraries
URL:        https://github.com/timoxley/columnify
Source0:    http://registry.npmjs.org/columnify/-/columnify-%{version}.tgz
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:  noarch
ExclusiveArch: %{nodejs_arches} noarch

BuildRequires:  nodejs-devel

%description
%{summary}.

%prep
%setup -q -n package

%build
#nothing to do

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{nodejs_sitelib}/columnify

cp -pr *.js package.json %{buildroot}%{nodejs_sitelib}/columnify


%nodejs_symlink_deps

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{nodejs_sitelib}/columnify
%doc Readme.md LICENSE

%changelog
* Sun Apr 12 2015 rommon <rommon@t-online.de> - 1.5.1-1
- initial package
