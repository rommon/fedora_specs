%{?nodejs_find_provides_and_requires}

Name:       nodejs-inflight
Version:    1.0.4
Release:    1%{?dist}
Summary:    Add callbacks to requests in flight to avoid async duplication
License:    BSD
Group:      System Environment/Libraries
URL:        https://github.com/npm/inflight
Source0:    http://registry.npmjs.org/inflight/-/inflight-%{version}.tgz
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:  noarch
ExclusiveArch: %{nodejs_arches} noarch

BuildRequires:  nodejs-devel

%description
%{summary}.

%prep
%setup -q -n package

%build
#nothing to do

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{nodejs_sitelib}/inflight
cp -pr *.js package.json %{buildroot}%{nodejs_sitelib}/inflight

%nodejs_symlink_deps

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{nodejs_sitelib}/inflight
%doc README.md LICENSE

%changelog
* Sun Apr 12 2015 rommon <rommon@t-online.de> - 1.0.4-1
- initial package
