%{?nodejs_find_provides_and_requires}

Name:       nodejs-path-is-inside
Version:    1.0.1
Release:    1%{?dist}
Summary:    Tests whether one path is inside another path
License:    MIT
Group:      System Environment/Libraries
URL:        https://github.com/domenic/path-is-inside
Source0:    http://registry.npmjs.org/path-is-inside/-/path-is-inside-%{version}.tgz
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:  noarch
ExclusiveArch: %{nodejs_arches} noarch

BuildRequires:  nodejs-devel

%description
Tests whether one path is inside another path.

%prep
%setup -q -n package

%build
#nothing to do

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{nodejs_sitelib}/path-is-inside
cp -pr lib package.json %{buildroot}%{nodejs_sitelib}/path-is-inside

%nodejs_symlink_deps

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{nodejs_sitelib}/path-is-inside
%doc README.md LICENSE.txt

%changelog
* Sun Apr 12 2015 rommon <rommon@t-online.de> - 1.0.1-1
- initial package
