%{?nodejs_find_provides_and_requires}

Name:       nodejs-mkdirp
Version:    0.5.0
Release:    1%{?dist}
Summary:    Recursive directory creation module for Node.js
License:    MIT
Group:      Development/Libraries
URL:        https://github.com/substack/node-mkdirp
Source0:    http://registry.npmjs.org/mkdirp/-/mkdirp-%{version}.tgz
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:  noarch
ExclusiveArch: %{nodejs_arches} noarch

BuildRequires:  nodejs-devel

%description
Creates directories recursively, like `mkdir -p`.

%prep
%setup -q -n package

%build
#nothing to do

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{nodejs_sitelib}/mkdirp
cp -pr index.js package.json bin %{buildroot}%{nodejs_sitelib}/mkdirp

%nodejs_symlink_deps

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{nodejs_sitelib}/mkdirp
%doc readme.markdown examples LICENSE

%changelog
* Sun Apr 12 2015 rommon <rommon@t-online.de> - 0.5.0-1
- update to 0.5.0

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.5-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.5-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Sat Jun 22 2013 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.3.5-3
- restrict to compatible arches

* Mon Apr 15 2013 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.3.5-2
- add macro for EPEL6 dependency generation

* Wed Mar 13 2013 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.3.5-1
- new upstream release 0.3.5

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.4-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Tue Jan 08 2013 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.3.4-2
- add missing build section
- improve summary/description

* Mon Dec 31 2012 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.3.4-1
- new upstream release 0.3.4
- clean up for submission

* Wed May 02 2012 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.3.2-1
- New upstream release 0.3.2

* Fri Apr 27 2012 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.3.1-2
- guard Requires for F17 automatic depedency generation

* Mon Apr 02 2012 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.3.1-1
- New upstream release 0.3.1

* Sat Jan 21 2012 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.3.0-1
- new upstream release 0.3.0

* Thu Dec 22 2011 T.C. Hollingsworth <tchollingsworth@gmail.com> - 0.2.1-1
- initial package
