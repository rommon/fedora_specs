# don't strip bundled binaries because pycharm checks length (!!!) of binary fsnotif
# and if you strip debug stuff from it, it will complain
%global __strip /bin/true
# dont repack jars
%global __jar_repack %{nil}

%if 0%{?rhel} <= 7
%bcond_with python3
%else
%bcond_without python3
%endif

Name:          intellij-idea-community
Version:       14.1.2
Release:       1%{?dist}
Summary:       Intelligent Java IDE
Group:         Development/Tools
License:       ASL 2.0
URL:           http://www.jetbrains.com/pycharm/
Source0:       http://download.jetbrains.com/idea/ideaIC-%{version}.tar.gz
Source1:       intellij-idea-community.xml
Source2:       intellij-idea-community.desktop
Source3:       intellij-idea-community.appdata.xml
BuildRequires: desktop-file-utils
Requires:      java

%description
Lightweight IDE for Java SE, Groovy & Scala development
Powerful environment for building Google Android apps
Integration with JUnit, TestNG, popular SCMs, Ant & Maven
Free

%prep
%setup -q -n idea-IC-141.713.2

%install
mkdir -p %{buildroot}%{_javadir}/%{name}
# datadir is not necessary
mkdir -p %{buildroot}%{_datadir}/%{name}
mkdir -p %{buildroot}%{_datadir}/pixmaps
mkdir -p %{buildroot}%{_datadir}/mime/packages
mkdir -p %{buildroot}%{_datadir}/applications
mkdir -p %{buildroot}%{_datadir}/appdata
mkdir -p %{buildroot}%{_bindir}


# remove the 32/64 bit version of fsnotifier to have a clean installation
%ifarch %ix86
rm ./bin/fsnotifier64
rm ./bin/idea64.vmoptions
rm ./bin/libbreakgen64.so
%endif

%ifarch x86_64 amd64
rm ./bin/fsnotifier
rm ./bin/idea.vmoptions
rm ./bin/libbreakgen.so
%endif

cp -arf ./{lib,bin,plugins} %{buildroot}%{_javadir}/%{name}/
# this will be in docs
rm -f %{buildroot}%{_javadir}/help/*.pdf
cp -af ./bin/idea.png %{buildroot}%{_datadir}/pixmaps/%{name}.png
cp -af %{SOURCE1} %{buildroot}%{_datadir}/mime/packages/%{name}.xml
cp -af %{SOURCE2} %{buildroot}%{_datadir}/intellij-idea-community.desktop
cp -a %{SOURCE3} %{buildroot}%{_datadir}/appdata
ln -s %{_javadir}/%{name}/bin/idea.sh %{buildroot}%{_bindir}/intellij-idea-community
ln -s %{_javadir}/%{name}/bin/idea.sh %{buildroot}%{_bindir}/intellij
ln -s %{_javadir}/%{name}/bin/idea.sh %{buildroot}%{_bindir}/idea
desktop-file-install                          \
--add-category="Development"                  \
--delete-original                             \
--dir=%{buildroot}%{_datadir}/applications    \
%{buildroot}%{_datadir}/intellij-idea-community.desktop

%files
%defattr(-,root,root)
%doc *.txt
%doc license/
# datadir is not necessary
%dir %{_datadir}/%{name}
%dir %{_javadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/mime/packages/%{name}.xml
%{_datadir}/pixmaps/intellij-idea-community.png
%{_datadir}/appdata/intellij-idea-community.appdata.xml
%{_javadir}/%{name}/*
%{_bindir}/intellij-idea-community
%{_bindir}/intellij
%{_bindir}/idea


%changelog
* Thu Apr 23 2015 Rommon <rommon@t-online.de> - 14.1.2-1
- Initial package
