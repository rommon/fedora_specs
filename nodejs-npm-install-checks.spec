%{?nodejs_find_provides_and_requires}

Name:       nodejs-npm-install-checks
Version:    1.0.5
Release:    1%{?dist}
Summary:    A package that contains checks that npm runs during the installation
License:    MIT
Group:      System Environment/Libraries
URL:        https://github.com/npm/npm-install-checks
Source0:    http://registry.npmjs.org/npm-install-checks/-/npm-install-checks-%{version}.tgz
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:  noarch
ExclusiveArch: %{nodejs_arches} noarch

BuildRequires:  nodejs-devel

%description
A package that contains checks that npm runs during the installation.

%prep
%setup -q -n package

%build
#nothing to do

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{nodejs_sitelib}/npm-install-checks
cp -pr index.js package.json %{buildroot}%{nodejs_sitelib}/npm-install-checks

%nodejs_symlink_deps

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{nodejs_sitelib}/npm-install-checks
%doc README.md LICENSE

%changelog
* Sun Apr 12 2015 rommon <rommon@t-online.de> - 1.0.5-1
- initial package
